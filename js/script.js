$(function(){

	$("#recommended-carts-slider").owlCarousel({
		center: true,
		nav: true,
		autoWidth: true,
		margin: -41,
		loop: true,
		autoplay: false,
		autoplayTimeout: 4000,
		navContainerClass: 'navigation',
		navClass: ['customPrev', 'customNext'],
		navText: ['<img src="img/arrow-prev.png" alt="prev arrow">', '<img src="img/arrow-next.png" alt="next arrow">'],
		responsive : {
			0: {
				items: 1,
				margin: -200
			},

		    992 : {
		    	items: 3,
		        margin: -140
		    },

		    1200 : {
		        margin: -41
		    }
		}

	});

	$("#main-slider").owlCarousel({
		items: 1,
		center: true,
		loop: true,
		// autoplay: true,
		autoplayTimeout: 4000
	});

	var checkWidth = $(document).width();

	if(checkWidth <576){
		$("#popular-products-slider").owlCarousel({
			items: 1,
			dots: false,
			margin: 0,
			nav: true,
			navContainerClass: 'navigation',
			navClass: ['customPrev', 'customNext'],
			navText: ['<img src="img/arrow-prev.png" alt="prev arrow">', '<img src="img/arrow-next.png" alt="next arrow">'],
		});

		$("#recommended-products-slider").owlCarousel({
			items: 1,
			dots: false,
			margin: 0,
			nav: true,
			navContainerClass: 'navigation',
			navClass: ['customPrev', 'customNext'],
			navText: ['<img src="img/arrow-prev.png" alt="prev arrow">', '<img src="img/arrow-next.png" alt="next arrow">'],
		});
	}

});